---
name: "Raspberry Pi"
comment: "experimental"
deviceType: "tv"
installLink: "https://gitlab.com/ubports/community-ports/raspberrypi"
maturity: .1
buyLink: "https://www.raspberrypi.org/products/"
---

The [Raspberry Pi](https://www.raspberrypi.org/) is an affordable ARM-based linux single-board computer. An [experimental Ubuntu Touch image](https://ci.ubports.com/job/rootfs/job/rootfs-rpi/) is available.
